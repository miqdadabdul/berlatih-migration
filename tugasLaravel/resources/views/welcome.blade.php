@extends('layout.master')

@section('judul')
Halaman Welcome
@endsection

@section('content')

    <h1>SELAMAT DATANG! {{$namadpn}} {{$namablkg}}</h1>
    <h3>Terima kasih telah bergabung di Website Kami. Media Belajar kita bersama!</h3>

@endsection